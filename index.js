const TelegramBot = require('node-telegram-bot-api');
const firebase = require('firebase');
const tinfo = require('./tokens/telegram.js');
const firebaseinfo = require('./tokens/firebaseinfo.js')

var Sleeps

const token = tinfo.token;
const bot = new TelegramBot(token, {polling: true});


const app = firebase.initializeApp(firebaseinfo);

/* function getChildren(childKey, username, firebase) {
    return new Promise((resolve, reject) => {
        firebase.database().ref('users/' + username).once('value', (snapshot) => {
            var childRef = snapshot.child(childKey)
            console.log(childRef.val());
            console.log(childRef.numChildren());
            console.log(childRef.hasChild('0'));
            console.log(childRef.key);
            resolve(snapshot.child(childKey).numChildren());
        });
    })
} */

function writeEat(username, date) {
    let dataRef = firebase.database().ref('ate/' + username);
    dataRef.push(date)
}

function writeSleep(username, date) {
    let dataRef = firebase.database().ref('sleep/' + username);
    dataRef.push(date)
}


bot.onText(/\/Söin/, msg => {
    bot.sendMessage(msg.chat.id, 'Kirjataan...');
    writeEat(msg.from.username, msg.date);
});


bot.onText(/\/Uni/, msg => {
    bot.sendMessage(msg.chat.id, 'Kirjataan...');
    writeSleep(msg.from.username, msg.date);
});